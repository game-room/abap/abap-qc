CLASS zcl_jega_profiling_2 DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_oo_adt_classrun .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS zcl_jega_profiling_2 IMPLEMENTATION.

  METHOD if_oo_adt_classrun~main.
    DATA(lt_connections) = lcl_data=>get_connections( ).
    SORT lt_connections BY carrier_id ASCENDING connection_id ASCENDING.

    LOOP AT lt_connections ASSIGNING FIELD-SYMBOL(<fs_conn>).
      DATA(lv_city_f) = lcl_data=>get_airport_city( <fs_conn>-airport_from_id ).
      DATA(lv_city_t) = lcl_data=>get_airport_city( <fs_conn>-airport_to_id ).
      out->write(  '----------------------------------------------------------------------------------------------' ).
      out->write( |Record number: { sy-tabix } --> | && | Flight { <fs_conn>-carrier_id } { <fs_conn>-connection_id } from { lv_city_f } to { lv_city_t } | ).
    ENDLOOP.
    out->write(  '-----------------------------------------------------------------------------------------------' ).

  ENDMETHOD.
ENDCLASS.
