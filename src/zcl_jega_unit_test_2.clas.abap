CLASS zcl_jega_unit_test_2 DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.

    INTERFACES if_oo_adt_classrun .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS zcl_jega_unit_test_2 IMPLEMENTATION.


  METHOD if_oo_adt_classrun~main.
    CONSTANTS c_carrier_id TYPE /dmo/carrier_id VALUE 'AA'.
    " CONSTANTS c_carrier_id TYPE /dmo/carrier_id VALUE 'XX'.
    TRY.
        DATA(carrier) = NEW lcl_carrier( i_carrier_id = c_carrier_id ).
        DATA(name) = carrier->get_name( ).
        DATA(currency) = carrier->get_currency(  ).
        out->write( |Carrier { name } has currency { currency }| ).
      CATCH cx_abap_invalid_value.
        out->write( |Carrier ID: { c_carrier_id } does not exist!| ).
    ENDTRY.
  ENDMETHOD.
ENDCLASS.
