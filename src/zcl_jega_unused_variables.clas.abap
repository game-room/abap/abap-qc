CLASS zcl_jega_unused_variables DEFINITION
  PUBLIC
  FINAL
  CREATE PUBLIC .

  PUBLIC SECTION.
    INTERFACES if_oo_adt_classrun .
  PROTECTED SECTION.
  PRIVATE SECTION.
ENDCLASS.



CLASS zcl_jega_unused_variables IMPLEMENTATION.
  METHOD if_oo_adt_classrun~main.
    " Right Click - Run as - Abap Test Cockpit With
    " DATA carrier_list TYPE TABLE OF /dmo/carrier.
    " DATA connection_list TYPE TABLE OF /dmo/connection.

    " ATC finding not suppressed
    DATA text1 TYPE string.

    " ATC finding suppressed using Pseudo Comment
    DATA text2 TYPE string.                                 "#EC NEEDED

    " ATC finding suppressed using Pragma
    DATA text3 TYPE string ##needed.

    SELECT FROM /dmo/connection
        FIELDS *
        INTO TABLE @DATA(connections).

    " connection_list = connection_list.
    " out->write( connection_list ).
    " out->write( 'Hello World' ).
    out->write( connections ).
  ENDMETHOD.
ENDCLASS.
