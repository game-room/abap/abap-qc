*"* use this source file for your ABAP unit test classes
CLASS ltcl_test DEFINITION FINAL FOR TESTING
  DURATION SHORT
  RISK LEVEL HARMLESS.

  PRIVATE SECTION.
    METHODS test_get_name FOR TESTING RAISING cx_static_check.
    METHODS test_get_currency FOR TESTING.
    METHODS setup.
    DATA carrier TYPE REF TO lcl_carrier.
ENDCLASS.


CLASS ltcl_test IMPLEMENTATION.
  METHOD setup.
    SELECT SINGLE FROM /dmo/carrier FIELDS carrier_id INTO @DATA(carrier_id).

    IF sy-subrc <> 0.
      cl_abap_unit_assert=>skip(
        msg = 'No recrods exist in /dmo/carrier'
        detail = 'There should be atleast one record in the table /dmo/carrier to run the tests' ).
    ENDIF.

    TRY.
        me->carrier = NEW lcl_carrier( carrier_id ).
      CATCH cx_abap_invalid_value.
        cl_abap_unit_assert=>fail( 'Constructor threw a cx_abap_invalid_value exception unexpectedly' ).
    ENDTRY.

    cl_abap_unit_assert=>assert_bound(
        EXPORTING act = me->carrier
        msg = 'constructor failed' ).
  ENDMETHOD.

  METHOD test_get_name.
    cl_abap_unit_assert=>assert_not_initial(
        act = me->carrier->get_name(  )
        msg = 'Result of method get_name()'
        quit = if_abap_unit_constant=>quit-no ).
  ENDMETHOD.

  METHOD test_get_currency.
    cl_abap_unit_assert=>assert_not_initial(
        act = me->carrier->get_currency(  )
        msg = 'Result of method get_currency()'
        quit = if_abap_unit_constant=>quit-no ).
  ENDMETHOD.
ENDCLASS.
