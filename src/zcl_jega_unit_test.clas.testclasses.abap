*"* use this source file for your ABAP unit test classes
CLASS ltcl_test DEFINITION FINAL FOR TESTING
  DURATION SHORT
  RISK LEVEL HARMLESS.

  PRIVATE SECTION.
    METHODS:
      test_success FOR TESTING RAISING cx_static_check,
      test_exception FOR TESTING.

ENDCLASS.


CLASS ltcl_test IMPLEMENTATION.

  METHOD test_success.
    "Preparation: Read an arbitrary carrier_id from DB
    SELECT SINGLE
    FROM /dmo/carrier
    FIELDS carrier_id
    INTO @DATA(lv_carrier_id).
    IF sy-subrc IS INITIAL.
**********************************************************************
      "Do the test of method lcl_data=>get_carrier( )
**********************************************************************
      TRY.
          DATA(carrier_id) = lcl_data=>get_carrier( lv_Carrier_id ).
        CATCH cx_abap_invalid_value.
          cl_abap_unit_assert=>fail( msg    = 'Inapropriate exception'(001)
                                     detail = 'Method lcl=>get_data( ) raises an exception when it should not' ).
      ENDTRY.
    ELSE.
**********************************************************************
      "No data in table /DMO/CARRIER, stop testing
**********************************************************************
      cl_abap_unit_assert=>fail( 'Test requires at least one entry in DB table /DMO/CARRIER' ).
    ENDIF.


  ENDMETHOD.

  METHOD test_exception.
    "Specify a carrier_id that does not exist on DB
    CONSTANTS lc_wrong_carrier TYPE /dmo/carrier_id VALUE 'XX'.

    "Preparation: Make sure that the carrier_id does not exist on DB
    SELECT SINGLE
    FROM /dmo/carrier
    FIELDS carrier_id
    WHERE carrier_id = @lc_wrong_carrier
    INTO @DATA(lv_dummy).
    IF sy-subrc IS INITIAL.
**********************************************************************
      "Carrier exists on DB /DMO/CARRIER, stop testing
**********************************************************************
      cl_abap_unit_assert=>fail( msg    = | Carrier { lc_wrong_carrier } exists in /DMO/CARRIER  |
                                 level  = if_abap_unit_constant=>severity-medium
                                 quit   = if_abap_unit_constant=>quit-test
                                 detail = `If DB table /DMO/CARRIER contains an entry` && |with carrier_id = '{ lc_wrong_carrier } '| &&
                                          `its not possible to test the exception` ).
    ELSE.
**********************************************************************
      "Do the test of lcl_data=>get_carrier( ), exception expected
**********************************************************************
      TRY.
          DATA(carrier_id) = lcl_data=>get_carrier( lc_wrong_carrier ).

          "This code should not be reach
          cl_abap_unit_assert=>fail( msg    = 'No exception'
                                     detail = 'Method lcl=>get_data( ) does not raise an exception when it should' ).
        CATCH cx_abap_invalid_value.

      ENDTRY.
    ENDIF.
  ENDMETHOD.

ENDCLASS.
