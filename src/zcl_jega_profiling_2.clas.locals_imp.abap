*"* use this source file for the definition and implementation of
*"* local helper classes, interface definitions and type
*"* declarations
CLASS lcl_data DEFINITION.

  PUBLIC SECTION.
    CLASS-METHODS:
      get_connections RETURNING VALUE(rt_connections) TYPE ztt_DMO_Connections,
      get_airport_city
        IMPORTING i_airport     TYPE /dmo/airport_id
        RETURNING VALUE(r_city) TYPE /dmo/city.

  PROTECTED SECTION.
  PRIVATE SECTION.

ENDCLASS.

CLASS lcl_data IMPLEMENTATION.

  METHOD get_connections.

    SELECT
      FROM /dmo/connection
      FIELDS client, carrier_id, connection_id, airport_from_id, airport_to_id
      INTO TABLE @rt_connections
      UP TO 100 ROWS.

  ENDMETHOD.

  METHOD get_airport_city.
    SELECT SINGLE
      FROM /dmo/airport
      FIELDS city
      WHERE airport_id = @i_airport
      INTO @r_city.
  ENDMETHOD.

ENDCLASS.
